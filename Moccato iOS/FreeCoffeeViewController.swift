//
//  FreeCoffeeViewController.swift
//  Moccato iOS
//
//  Created by Bjarki Sorens on 25/02/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import UIKit

class FreeCoffeeViewController: UIViewController {

    @IBOutlet weak var tokenInput: UITextField!
    @IBOutlet weak var amountSegment: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func checkTokenButtonPushed(sender: UIButton) {
        
        let hud = Loading.showLoadingInView(self.view, withText: "Verifying code")
        self.tokenInput.resignFirstResponder() // hide keyboard
        
        var amountOfCups = self.requestedAmountOfCupsFromSegment()
        if amountOfCups == 0 {
            return
        }
        
        Factory.getBackendService().verifyToken(self.tokenInput.text, callback: {
            (tokenVerified: Bool) in
            if tokenVerified {
                Factory.getBackendService().purchaseCoffeeCups(amountOfCups, callback: {(purchaseComplete: Bool) in
                    Loading.hideLoading(hud)
                    if purchaseComplete {
                        self.performSegueWithIdentifier("purchaseComplete", sender: self)
                    } else {
                        Loading.showError(self.view, withText: "Unable to purchase!")
                    }
                })
                
            } else {
                Loading.hideLoading(hud)
                Loading.showError(self.view, withText: "Invalid token!")
            }
        })
    }
    
    
    // MARK: - Private functions
    
    private func requestedAmountOfCupsFromSegment() -> Int {
        switch self.amountSegment.selectedSegmentIndex {
        case 0:
            return 1
        case 1:
            return 2
        default:
            return 0
        }
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "purchaseComplete" {
            let vc = segue.destinationViewController as! CertificateViewController
            vc.amountOfCups = self.requestedAmountOfCupsFromSegment()
        }
    }
    

}
