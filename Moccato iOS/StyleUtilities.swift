//
//  StyleUtilities.swift
//  Moccato iOS
//
//  Created by Filip Strycko on 06/05/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import Foundation
import UIKit

struct StyleUtilities {
    
    static let moccatoNavigationBarTintColor:UIColor = UIColor(red: 40/255, green: 40/255, blue: 40/255, alpha:1.0 )
    
    static let moccatoLightGreyColor:UIColor = UIColor(red: 176/255, green: 176/255, blue: 176/255, alpha: 1.0)
    
    static let moccatoVeryLightGreyColor:UIColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0)
    
    static let moccatoVeryVeryLightGreyColor:UIColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1.0)

}

