//
//  Flavor.swift
//  Moccato iOS
//
//  Created by Bjarki Sorens on 02/05/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import Foundation

class Flavor {
    var id: Int?
    var name: String?
    var description: String?
    var active: Bool?
}