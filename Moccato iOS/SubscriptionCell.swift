//
//  SubscriptionCell.swift
//  Moccato iOS
//
//  Created by Filip Strycko on 09/05/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import UIKit

class SubscriptionCell: UITableViewCell {

    
    public var subscriptionName:String = ""
    var subscriptionDescription:String = ""
    var subscriptionId:Int = 0
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    
    }
    
    override func layoutSubviews() {
        let shapeRect = CGRectMake(4, 6, self.frame.width-16, self.frame.height-17)
        let maskPath = UIBezierPath(roundedRect: shapeRect, byRoundingCorners: UIRectCorner.TopLeft | UIRectCorner.TopRight | UIRectCorner.BottomLeft | UIRectCorner.BottomRight , cornerRadii: CGSizeMake(1.5, 1.5))

        let shapeRectTop = CGRectMake(4, 6, self.frame.width-16, 32)
        let maskPathTop = UIBezierPath(roundedRect: shapeRectTop, byRoundingCorners: UIRectCorner.TopLeft | UIRectCorner.TopRight , cornerRadii: CGSizeMake(1.5, 1.5))

        let shape = CAShapeLayer()
        shape.frame = shapeRect
        shape.path = maskPath.CGPath
        shape.strokeColor = nil
        shape.fillColor = UIColor.whiteColor().CGColor
        //shadow
        shape.shadowColor = StyleUtilities.moccatoVeryLightGreyColor.CGColor
        shape.shadowOffset = CGSizeMake(0.0, 2.0)
        shape.shadowOpacity = 1.0
        shape.shadowPath = maskPath.CGPath
        shape.shadowRadius = 1.5
        
        
        let shapeTop = CAShapeLayer()
        shapeTop.frame = shapeRectTop
        shapeTop.path = maskPathTop.CGPath
        //shapeTop.strokeColor = UIColor.lightGrayColor().CGColor
        shapeTop.strokeColor = nil
        shapeTop.fillColor = StyleUtilities.moccatoVeryVeryLightGreyColor.CGColor
        
        //bottom border
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRectMake(shapeRectTop.origin.x, shapeRectTop.origin.y + shapeRectTop.size.height, shapeRectTop.size.width, 1)
        bottomBorder.backgroundColor = StyleUtilities.moccatoVeryLightGreyColor.CGColor
        shapeTop.addSublayer(bottomBorder)
        
        
        self.layer.addSublayer(shape)
        self.layer.addSublayer(shapeTop)
        
        
        //name of subscription label
        var nameLabelRect = shapeRectTop
        nameLabelRect.origin.x = 20
        nameLabelRect.origin.y = 12
        
        let nameLabel = UILabel(frame: nameLabelRect)
        nameLabel.text = self.subscriptionName
        nameLabel.font = UIFont(name: "HelveticaNeue", size: 17)
        nameLabel.textColor = UIColor.blackColor()
        self.addSubview(nameLabel)
        
        
        //description label
        var descLabelRect = CGRectMake(20, 37, shapeRect.size.width-20, shapeRect.size.height-20)
        
        var paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 5
        var attrString = NSMutableAttributedString(string: self.subscriptionDescription)
        attrString.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range:NSMakeRange(0, attrString.length))

        let descLabel = UILabel(frame: descLabelRect)
        descLabel.attributedText = attrString
        descLabel.numberOfLines = 0
        descLabel.font = UIFont(name: "HelveticaNeue", size: 13)
        descLabel.textColor = StyleUtilities.moccatoNavigationBarTintColor
        self.addSubview(descLabel)
        
    }

//    override func dra
}
