//
//  CoffeeShopTableViewCell.swift
//  Moccato iOS
//
//  Created by Bjarki Sorens on 25/02/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import UIKit

class CoffeeShopTableViewCell: UITableViewCell {

    
    @IBOutlet weak var cafeteriaName: UILabel!
    @IBOutlet weak var cafeteriaAddress: UILabel!
    @IBOutlet weak var cafeteriaDistance: UILabel!
    @IBOutlet weak var cafeteriaThumbnailImage: UIImageView!
    @IBOutlet weak var cafeteriaBookmarkImage: UIImageView!
    @IBOutlet weak var ratingSquare: RatingSquare!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
