//
//  Datamanager.swift
//  Moccato iOS
//
//  Created by Bjarki Sorens on 25/02/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import UIKit
import Foundation
import CoreData

public class Datamanager {
    
    // constants
    let cafeteriaEntityName = "Cafeteria"
    let userEntityName = "User"
    
    var managedObjectContext: NSManagedObjectContext?
    
    public init(managedObjectContext: NSManagedObjectContext) {
        self.managedObjectContext = managedObjectContext
    }
    
    // Makes a fetch to Core Data for all stored coffee shops
    public func fetchCafeterias() -> [Cafeteria] {
        let fetchRequest = NSFetchRequest(entityName: self.cafeteriaEntityName)
        let sortDescriptor = NSSortDescriptor(key: "id", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        if let fetchResult = self.managedObjectContext!.executeFetchRequest(fetchRequest, error: nil) as? [Cafeteria] {
            return fetchResult
        }
        // maybe some error handling ?
        return []
    }
    
    public func fetchCafeteriasGivenSearchText(searchText: String) -> [Cafeteria]? {
        let fetchRequest = NSFetchRequest(entityName: self.cafeteriaEntityName)
        let predicate = NSPredicate(format: "name CONTAINS[c] %@", searchText)
        fetchRequest.predicate = predicate
        
        if let fetchResult = self.managedObjectContext!.executeFetchRequest(fetchRequest, error: nil) as? [Cafeteria] {
            return fetchResult
        }
        return nil
    }
    
    // Makes a fetch to Core Data for all stored coffee shops
    public func fetchFavourited() -> [Cafeteria] {
        if let user = self.fetchCurrentUserData() {
            let cs = user.favoriteCafeterias.allObjects.map{
                (var object) -> Cafeteria in
                return object as! Cafeteria
            }
            return cs
        }
        
        return []
    }
    
    
    // MARK: User
    
    public func saveUserInformation(first_name: String, email: String) {
        let user = NSEntityDescription.insertNewObjectForEntityForName("User", inManagedObjectContext: self.managedObjectContext!) as! User
        user.first_name = first_name
        user.email = email
    }
    
    /// Get current user data
    public func fetchCurrentUserData() -> User?{
        let fetchRequest = NSFetchRequest(entityName: self.userEntityName)
        if let fetchResult = self.managedObjectContext!.executeFetchRequest(fetchRequest, error: nil) as? [User] {
            if (fetchResult.count != 0) {
                return fetchResult.first
            }else{
                return nil
            }
            
        }
        // maybe some error handling ?
        return nil
    }
    
    public func saveReview(review: String, rating:Float, forCafeteria cafeteria: Cafeteria) {
        let cafeteriaScore = NSEntityDescription.insertNewObjectForEntityForName("CafeteriaScore", inManagedObjectContext: self.managedObjectContext!) as! CafeteriaScore
        
        // populate
        cafeteriaScore.user = self.fetchCurrentUserData()!
        cafeteriaScore.score = rating
        cafeteriaScore.text = review
        cafeteriaScore.cafeteria = cafeteria
        
    }
    
}