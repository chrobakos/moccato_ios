//
//  Loading.swift
//  Moccato iOS
//
//  Created by Bjarki Sorens on 27/02/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import Foundation
import MBProgressHUD

class Loading {
    
    class func showLoadingInView(view:UIView, withText text:String) -> MBProgressHUD {
        let hud = MBProgressHUD.showHUDAddedTo(view, animated: true)
        hud.mode = MBProgressHUDModeIndeterminate
        hud.labelText = text
        return hud
    }
    
    class func hideLoading(hud:MBProgressHUD) {
        dispatch_async(dispatch_get_main_queue()) {
            hud.hide(true)
        }
    }
    
    class func showSuccess(view:UIView, withText text:String, hideAfterDelay delay:Double){
        let hud = MBProgressHUD.showHUDAddedTo(view, animated: true)
        hud.customView = UIImageView(image: UIImage(named: "Checkmark"))
        hud.mode = MBProgressHUDModeCustomView
        hud.labelText = text
        hud.show(true)
        hud.hide(true, afterDelay: delay)
    }
    
    class func showSuccess(view:UIView, withText text:String) {
        self.showSuccess(view, withText: text, hideAfterDelay: 3)
    }
    
    class func showError(view:UIView, withText text:String) {
        let hud = MBProgressHUD.showHUDAddedTo(view, animated: true)
        hud.customView = UIImageView(image: UIImage(named: "Error"))
        hud.mode = MBProgressHUDModeCustomView
        hud.labelText = text
        hud.show(true)
        hud.hide(true, afterDelay: 3)
    }
}
