//
//  UserAccountCell.swift
//  Moccato iOS
//
//  Created by Filip Strycko on 09/05/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import UIKit

class UserAccountCell: UITableViewCell {

    @IBOutlet weak var userProfileImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userEmailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }
    override func layoutSubviews() {
        self.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        //bottom border
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRectMake(0, self.frame.size.height, self.frame.size.width, 1)
        bottomBorder.backgroundColor = StyleUtilities.moccatoVeryLightGreyColor.CGColor
        self.layer.addSublayer(bottomBorder)
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
