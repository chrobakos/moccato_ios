//
//  Subscription.swift
//  Moccato iOS
//
//  Created by Bjarki Sorens on 02/05/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import Foundation

class Subscription {
    var id: Int?
    var user: User?
    var package: Package?
    var expiration:NSDate?
    var updatedAt: NSDate?
    var createdAt: NSDate?
}