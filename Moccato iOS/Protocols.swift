//
//  Protocols.swift
//  Moccato iOS
//
//  Created by Bjarki Sorens on 22/04/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import Foundation
import UIKit
import ReactiveCocoa

protocol DynamicCellHeight {
    /**
    * Configures a a basic cell that should be in the table at a certain index path. Used
    * to configure the sizing cell before calculating the height.
    */
    func configureBasicCell(var cell: UITableViewCell, atIndexPath indexPath: NSIndexPath)
    /**
    * After configuration, this function should calculate the height of the sizing cell.
    */
    func heightForCellAtIndexPath(indexPath:NSIndexPath) -> CGFloat
}

protocol BackendService {
    
    func getCafeterias(callback: (cafeterias: [Cafeteria]?) -> ())
    
    func getSubscriptionTypes(callback: (subscriptionTypes: [SubscriptionType]?) -> ())
    
    //func getSubscription(subscriptionId: Int, callback: (subscription: Subscription?) -> ())
    
    func getPackages(callback: (packages: [Package]?) -> ())
    
    func getFlavors(callback: (flavors: [Flavor]?) -> ())
    
    func verifyToken(token:String, callback: (tokenVerified: Bool) -> ())
    
    func loginUserWithEmail(email: String, password: String) -> RACSignal
    
    func getUser(user_id: Int) -> RACSignal
    
    func getUsersSubscriptions() ->RACSignal
    
    func registerNewUser(name: String, surname: String, email:String, password:String) -> RACSignal
    
    func getPaymentAndOrderHistoryOfUser(user_id:Int) -> RACSignal
    
    func purchaseCoffeeCups(numberOfCups: Int, callback: (purchaseComplete: Bool) -> ())
    
    func rateAndReviewCafeteria(cafeteriaId: Int, review: String, rating: Float, callback: (success: Bool) -> ())
}

/**
 * When the user logs in and out, we might want to notify other components of that event.
 */
protocol LoginLogoutDelegate {
    /**
     * This function will be called when the user successfully finishes logging in via a HTTP request.
     * param: viewController - the view controller that the user logged in from and should be dismissed.
     */
    func userDidFinishLogin(viewController: UIViewController)
    
    /**
     * Will be called when the user successfully logs out.
     */
    func userDidFinishLogOut()
}