//
//  MapViewController.swift
//  Moccato iOS
//
//  Created by Disha on 19/03/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController, MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        var lat1:CLLocationDegrees = 49
        var lon1:CLLocationDegrees = 49
        
        
        var lat2:CLLocationDegrees = 600
        var lon2:CLLocationDegrees = 260
        
        
        var lat3:CLLocationDegrees = 900
        var lon3:CLLocationDegrees = 390
        
        
        var lat4:CLLocationDegrees = 1200
        var lon4:CLLocationDegrees = 520
        
        
        var lat5:CLLocationDegrees = 1500
        var lon5:CLLocationDegrees = 650
        
        var latDelta:CLLocationDegrees = 0.01
        var longDelta:CLLocationDegrees = 0.01
        
        var theSpan:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, longDelta)
        
        var coffeeShop1Location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(lat1, lon1)
        var coffeeShop2Location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(lat2, lon2)
        var coffeeShop3Location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(lat3, lon3)
        var coffeeShop4Location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(lat4, lon4)
        var coffeeShop5Location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(lat5, lon5)

        

        var theRegion1:MKCoordinateRegion = MKCoordinateRegionMake(coffeeShop1Location, theSpan)
        var theRegion2:MKCoordinateRegion = MKCoordinateRegionMake(coffeeShop2Location, theSpan)
        var theRegion3:MKCoordinateRegion = MKCoordinateRegionMake(coffeeShop3Location, theSpan)
        var theRegion4:MKCoordinateRegion = MKCoordinateRegionMake(coffeeShop4Location, theSpan)
        var theRegion5:MKCoordinateRegion = MKCoordinateRegionMake(coffeeShop5Location, theSpan)

        
        self.mapView.setRegion(theRegion1, animated: true)
        
    
            
            // Set map view delegate with controller
            self.mapView.delegate = self
            
            var coffeeShop = CLLocationCoordinate2DMake(49, 49)
            // Drop a pin
            var dropPin = MKPointAnnotation()
            dropPin.coordinate = coffeeShop
            dropPin.title = "SHOP#1"
            mapView.addAnnotation(dropPin)
        }
    
        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */




