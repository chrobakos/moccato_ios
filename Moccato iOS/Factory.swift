//
//  Factory.swift
//  Moccato iOS
// 
//  Factory that retrives custom configurations.
//
//  Created by Bjarki Sorens on 04/03/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class Factory {
    
    static let configFileName = "Configuration"
    static var sharedDataFetcher:BackendService?
    
    class func getConfigDictionary() -> NSDictionary? {
        var path = NSBundle.mainBundle().pathForResource(self.configFileName, ofType: "plist")
        return NSDictionary(contentsOfFile: path!)
    }
    
    class func getValidationBackendURL() -> String? {
        return self.getConfigDictionary()?.valueForKey("api_validation") as? String
    }
    
    class func getBackendURL() -> String? {
        return self.getConfigDictionary()?.valueForKey("api_moccato") as? String
    }
    
    class func getManagedObjectContext() -> NSManagedObjectContext {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        return appDelegate.managedObjectContext!
    }
    
    class func getBackendService() -> BackendService {
        if self.sharedDataFetcher == nil {
            var className = self.getConfigDictionary()?.valueForKey("backendService") as! String
            let aClass = NSClassFromString(className) as! NSObject.Type
            self.sharedDataFetcher = aClass() as? BackendService
        }
        return self.sharedDataFetcher!
    }
    
}
