//
//  Cafeteria.swift
//  
//
//  Created by Bjarki Sorens on 07/06/15.
//
//

import Foundation
import CoreData

@objc(Cafeteria)
public class Cafeteria: NSManagedObject {

    @NSManaged var contact_address: String
    @NSManaged var contact_name: String
    @NSManaged var created_at: NSDate
    @NSManaged var desc: String
    @NSManaged public var id: NSNumber
    @NSManaged var latitude: NSNumber
    @NSManaged var longitude: NSNumber
    @NSManaged public var name: String
    @NSManaged var phone: String
    @NSManaged var updated_at: NSDate
    @NSManaged var address: Address
    @NSManaged var favoritedBy: NSSet
    @NSManaged var ratings: NSSet

}
