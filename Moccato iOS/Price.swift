//
//  Price.swift
//  Moccato iOS
//
//  Created by Bjarki Sorens on 02/05/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import Foundation

class Price {
    var id: Int?
    var packId: Int?
    var fullPrice: Float?
    var podPrice: Float?
}