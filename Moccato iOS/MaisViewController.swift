//
//  MaisViewController.swift
//  Moccato iOS
//
//  Created by Disha on 18/05/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import Foundation
import UIKit

class MaisViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.

    }
    
   
    //static view table declaration
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let section = indexPath.section
        let row = indexPath.row
        
        //Rating Moccato at app store
        //for now it will take you to itunes
        if (indexPath.section == 2 && indexPath.row == 0) {
            var AppUrl: NSURL = NSURL(string: "https://www.apple.com/itunes/")!
            UIApplication.sharedApplication().openURL(AppUrl)
        println("section: \(section) and row: \(row)")
        }
        
        
        //like Moccato on Facebook
        if (indexPath.section == 2 && indexPath.row == 1) {
        var FBUrl: NSURL = NSURL(string: "https://www.facebook.com/pages/Moccato/1577027602539895")!
        UIApplication.sharedApplication().openURL(FBUrl)
        
        println("section: \(section) and row: \(row)")
        }
        
        
        //Follow Moccato on Instagram
        if (indexPath.section == 2 && indexPath.row == 2) {
            var InstaUrl: NSURL = NSURL(string: "https://instagram.com/moccatobr")!
            UIApplication.sharedApplication().openURL(InstaUrl)
        println("section: \(section) and row: \(row)")
        }
        
        if (indexPath.section == 1 && indexPath.row == 0) {
            var AppUrl: NSURL = NSURL(string: "http://moccato.com.br/")!
            UIApplication.sharedApplication().openURL(AppUrl)
            println("section: \(section) and row: \(row)")
        }

        
        if (indexPath.section == 1 && indexPath.row == 1) {
            var AppUrl: NSURL = NSURL(string: "http://moccato.com.br/")!
            UIApplication.sharedApplication().openURL(AppUrl)
            println("section: \(section) and row: \(row)")
        }

        
    }
    
    
}



