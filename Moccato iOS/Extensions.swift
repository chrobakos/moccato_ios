//
//  Extensions.swift
//  Moccato iOS
//
//  Created by Bjarki Sorens on 07/06/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import Foundation

public extension NSDate {
    
    public func certificateFormat() -> String {
        var formatter = NSDateFormatter()
        formatter.dateFormat = "dd.MM.yyyy - HH:MM"
        return formatter.stringFromDate(self)
    }
    
}