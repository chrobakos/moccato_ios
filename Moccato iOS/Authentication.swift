//
//  Authentication.swift
//  Moccato iOS
//
//  Created by Bjarki Sorens on 19/04/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import Foundation
import UIKit

class Authentication {
    
    private static let keychainItemWrapper = KeychainWrapper()
    private static let DEFAULT_TOKEN_KEY = "password"
    private static let dataManager = Datamanager(managedObjectContext: Factory.getManagedObjectContext())
    
    class func isUserLoggedIn() -> Bool {
        let token = self.keychainItemWrapper.myObjectForKey(kSecValueData) as? String
        let user = self.dataManager.fetchCurrentUserData()
        
        return token != DEFAULT_TOKEN_KEY && token != "" && user != nil
    }
    
    class func storeToken(token: String) {
        self.keychainItemWrapper.mySetObject(token, forKey: kSecValueData)
        self.keychainItemWrapper.writeToKeychain()
    }
    
    class func clearToken() {
        self.keychainItemWrapper.mySetObject("", forKey: kSecValueData)
        self.keychainItemWrapper.writeToKeychain()
    }
    
    class func getToken()->String?{
        if var token = self.keychainItemWrapper.myObjectForKey(kSecValueData) as? String{
            return token
        }else{
            return nil
        }
        
    }
}