//
//  CoffeeShopHeader.swift
//  Moccato iOS
//
//  Created by Bjarki Sorens on 20/04/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import UIKit

protocol CoffeeShopHeaderProtocol {
    func userChangedView(CoffeeShopViewSegment)
}

enum CoffeeShopViewSegment {
    case DetailView
    case CommentsView
}

class CoffeeShopHeader: UIView {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var cafeteriaImage: UIImageView!
    @IBOutlet weak var cafeteriaTitle: UILabel!
    @IBOutlet weak var cafeteriaAddress: UILabel!
    var delegate:CoffeeShopHeaderProtocol?
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

    @IBAction func userChangedSegmentedControl(sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            self.delegate?.userChangedView(.DetailView)
        case 1:
            self.delegate?.userChangedView(.CommentsView)
        default:
            return
        }
    }
    
}
