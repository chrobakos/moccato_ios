//
//  ServerApi.swift
//  Moccato iOS
//
//  Created by Filip Strycko on 07/03/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import Foundation
import ReactiveCocoa
import Alamofire
import SwiftyJSON

// TODO: Http status codes enum



class ServerApi{
    
    enum HttpMethod{
        case Post
        case Get
        case Put
    }
    
    internal var requestPath:String = ""
    internal var requestParams:[String: AnyObject] = [:]
    internal var requestMethod:HttpMethod = HttpMethod.Post
    
    internal var baseUrl:String = ""
    
    init(){}
    
    /// Function for defining custom HTTP headers which will be sent with every request
    func setupCustomHeaders() -> [String:String]?{ return [:] }

    
    
    /**
    Setting up connection and it's parameters. This configuration will be then used for all connections
    
    - Custom headers
    - Timeout interval
    
    - Whatever else

    
    :returns: Returns Manager shared instance.
    */
    func setupManager() ->Manager
    {
        var defaultHeaders = Manager.sharedInstance.session.configuration.HTTPAdditionalHeaders ?? [:]
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        
        //add custom headers, if available
        if let customHeaders = setupCustomHeaders(){
            for (key, value) in customHeaders
            {
                defaultHeaders[key] = value
            }
            configuration.HTTPAdditionalHeaders = defaultHeaders
            
        }
        
        configuration.timeoutIntervalForRequest = 30
        let manager = Manager(configuration: configuration)
        return manager
    }
    
    
    
    /**
    Creates signal which handles incoming response from the server. Uses RACReplaySubject, so multiple subscriptions wont cause multiple network calls.
    
    In this method is incoming response checked for HTTP status code and error and subsequently decided if request was successful or not. Payload of successful response is transformed into *Dictionary*. In case of unsuccessful response, particular *ServerApiError* is created and sent.
    
    :returns: Returns RACReplaySubject, so multiple subscriptions wont cause multiple network calls. In case of successful request and non-empty payload of response, *sendNext:* with *Dictionary* of payload is sent. If payload is empty, just *sendComplete:* is sent. In case of error, *sendError:* is sent with *ServerApiError* instance describing error.
    
    */
    func createHandlingSignal(request:Request) -> RACSignal
    {
        let subject:RACReplaySubject = RACReplaySubject()
        
        request.response { (request, response, responseValue, error) -> Void in
            let statusCode = response?.statusCode
            let responseString = NSString(data: responseValue as! NSData, encoding: NSUTF8StringEncoding)
            
            if(error == nil && statusCode == HTTPStatusCode.OK.rawValue){
                var responseValue:NSData? = responseValue as? NSData
                let responseDict = JSON(data:responseValue!)
                //if not response sent or if sent empty string
                if responseDict != nil {
                    let responseDictObj = responseDict.dictionaryObject
                    subject.sendNext(responseDictObj)
                    subject.sendCompleted()
                }else{
                    subject.sendCompleted()
                }
            }else{
                
                var apiError:NSError? = nil
                if let error:NSError = error {
                    
                    if (error.domain == "NSURLErrorDomain"){
                        //connection error
                        apiError = ServerApiError(errorType: ServerApiError.ErrorType.ConnectionError,
                            statusCode: statusCode,
                            localizedMessage: "Request error",
                            apiCode: nil,
                            error: error)
                    } else {
                        apiError = ServerApiError(errorType: ServerApiError.ErrorType.RequestError,
                            statusCode: statusCode,
                            localizedMessage: "Generic error - connection",
                            apiCode: nil,
                            error: error)
                    }
                    
                    
                }else{
                   
                    if (statusCode == HTTPStatusCode.Unauthorized.rawValue){
                        //renew token
                        apiError = ServerApiError(errorType: ServerApiError.ErrorType.InvalidAccessTokenError,
                            statusCode: HTTPStatusCode.Unauthorized.rawValue,
                            localizedMessage: "Invalid access token",
                            apiCode: nil,
                            error: error)
                    }else{
                        apiError = ServerApiError(errorType: ServerApiError.ErrorType.RequestError,
                            statusCode: statusCode,
                            localizedMessage: "Generic error - server",
                            apiCode: nil,
                            error: nil)

                    }
                    
                }
                
                subject.sendError(apiError)
            }
            
            
            
        }
        
        
        
        
        
        return subject;
    }
    
    // MARK: Requests
    
    /**
    Creates signal for `HTTP GET` request to particular path (of base url address, `self.baseUrl` ) with configuration of the framework (set in `self.setupManager()`)
    
    :param: path    path of URL address, where request should be sent
    
    :returns: handlingSignal (RACReplaySubject signal, defined in `self.createHandlingSignal()`)
    
    */
    func _get(path:String) -> RACSignal
    {
        //TODO: Renew token?
        
        let manager = setupManager()
        let response = manager.request(.GET, baseUrl+path, parameters: nil)
        
        return createHandlingSignal(response)
    }
    
    
    /**
    Creates signal for `HTTP POST` request to particular path (of base url address, `self.baseUrl` ) with paramteters and configuration of the framework (set in `self.setupManager()`)
    
    :param: path    path of URL address, where request should be sent
    :param: params  Dictionary of parameters, which will be sent as JSON parameters with the request
    
    :returns: handlingSignal (RACReplaySubject signal, defined in `self.createHandlingSignal()`)
    
    */
    func _post(path:String, params:[String:AnyObject]) -> RACSignal
    {
        //TODO: Renew token?
        
        let managerx = setupManager()
        let response = managerx.request(.POST, baseUrl+path, parameters: params, encoding: .JSON)
        
        return createHandlingSignal(response)
    }

    
    /**
    Creates signal for `HTTP PUT` request to particular path (of base url address, `self.baseUrl` ) with paramteters and configuration of the framework (set in `self.setupManager()`)
    
    :param: path    path of URL address, where request should be sent
    :param: params  Dictionary of parameters, which will be sent as JSON parameters with the request
    
    :returns: handlingSignal (RACReplaySubject signal, defined in `self.createHandlingSignal()`)
    
    */
    func _put(path:String, params:[String:AnyObject]) -> RACSignal
    {
        
        //TODO: Renew token?
        
        let managerx = setupManager()
        let response = managerx.request(.PUT, baseUrl+path, parameters: params, encoding: .JSON)
        
        return createHandlingSignal(response)
        
    }
    
    
    // MARK: HTTP Status Code
    
    enum HTTPStatusCode: Int {
        //Success
        
        /// OK - 200
        case OK = 200
        
        /// Created - 201
        case Created = 201
        
        /// Accepted - 202
        case Accepted = 202
        
        
        
        //Client error
        
        /// Bad Request - 400
        case BadRequest = 400
        
        /// Unauthorized - 401
        case Unauthorized = 401
        
        /// Forbidden - 403
        case Forbidden = 403
        
        /// Not Found - 404
        case NotFound = 404
        
        
        
        // Server error
        
        /// Internal Server Error - 500
        case InternalServerError = 500
        
        /// Not Implemented - 501
        case NotImplemented = 501
        
        /// Service Unavailable - 503
        case ServiceUnavailable = 503
        
        
        /// I'm a teapot - 418
        case ImATeapot = 418
        
    }
    
    
    
    
}

/// Subclass of NSError, contains error information of HTTP response.
public class ServerApiError : NSError{

    enum ErrorType:String {
        
        /// Connection to server couldnt be performed due to connectivity issue
        case ConnectionError = "ConnectionError"

        /// Access token sent with request was refused by server (invalid or expired access token)
        case InvalidAccessTokenError = "InvalidAccessTokenError"

        /// Error when incoming JSON doesn't conform to expected format
        case BadResponseFormatError = "BadResponseFormatError"
        
        /// Error caused by unsuccessful authorization. Typically during login or token renew
        case AuthError = "AuthError"
        
        /// Generic error for unsuccessful request
        case RequestError = "RequestError"
        
        case None = "None"
    }
    
    /// type of error
    var errorType:ErrorType?
    
    /// localized decription of error
    var localizedMessage:String?
    
    /// custon string to later identification of error
    var apiCode:String?
    
    /// HTTP status code
    var statusCode:Int?
    
    /// original error
    var error:NSError?
    
    init(errorType:ErrorType, statusCode:Int?, localizedMessage:String, apiCode:String?, error:NSError?){
        self.errorType = errorType
        self.statusCode = statusCode
        self.localizedMessage = localizedMessage
        self.apiCode = apiCode
        self.error = error
        
        if(error != nil){
            super.init(domain: error!.domain, code: error!.code, userInfo: error!.userInfo)
        }else{
            super.init(domain: "com.ServerApi.error", code: 0, userInfo: [NSLocalizedDescriptionKey:localizedMessage])
        }
        
    }

    required public init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    /// Dump the description of the instance with basic parameters
    override public var description: String {
        return "ServerApiError:\n\tErrorType: \(errorType!.rawValue) \n\tMessage: \(localizedMessage) \n\tApiCode: \(apiCode) \n\thttpCode: \(statusCode) \n\tError: \(error)"
    }
    
}

