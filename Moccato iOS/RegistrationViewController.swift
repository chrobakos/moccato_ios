//
//  RegistrationViewController.swift
//  Moccato iOS
//
//  Created by Bjarki Sorens on 17/04/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import UIKit

class RegistrationViewController: UIViewController, UINavigationBarDelegate {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var surnameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func positionForBar(bar: UIBarPositioning) -> UIBarPosition {
        return UIBarPosition.TopAttached
    }

    @IBAction func backButtonPushed(sender: UIBarButtonItem) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func submitButtonPushed(sender: AnyObject) {
        
        Factory.getBackendService().registerNewUser(self.nameTextField.text, surname: self.surnameTextField.text, email: self.emailTextField.text, password: self.passwordTextField.text).subscribeNext({ (responseDict) -> Void in
            
            Loading.showSuccess(self.view, withText: "Registration successful")
            self.dismissViewControllerAnimated(true, completion: nil)
            
            }, error: { (error) -> Void in
                
            Loading.showError(self.view, withText: "Registration error")
            self.dismissViewControllerAnimated(true, completion: nil)
        })
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
