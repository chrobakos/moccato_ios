//
//  CertificateViewController.swift
//  Moccato iOS
//
//  Created by Bjarki Sorens on 05/03/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import UIKit

class CertificateViewController: UIViewController, UIBarPositioningDelegate, UINavigationBarDelegate, UIActionSheetDelegate {
    
    // will be set by navigation
    var amountOfCups: Int?
    
    @IBOutlet weak var certificateAttributedText: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupCertificateText()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    
    // MARK: - IB Actions
    
    @IBAction func dismissPressed(sender: UIButton) {
        var sheet = UIActionSheet(
            title: "Have you gotten your free coffee using this certificate?",
            delegate: self,
            cancelButtonTitle: "No",
            destructiveButtonTitle: "Yes, I have received my free coffee")
        sheet.showInView(self.view)
    }
    
    
    // MARK: - Delegation Events
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == actionSheet.destructiveButtonIndex {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    // MARK: - Private functions
    
    private func setupCertificateText() {
        var pluralString = self.amountOfCups! > 1 ? "s" : ""
        var now = NSDate()
        var boldFont = UIFont(name: "HelveticaNeue-Bold", size: 13.0)!
        
        var numberOfCupsString = NSMutableAttributedString(string: "\(self.amountOfCups!) café\(pluralString)", attributes: [NSFontAttributeName: boldFont])
        
        var dateOfPurchaseString = NSMutableAttributedString(string: now.certificateFormat(), attributes: [NSFontAttributeName: boldFont])
        
        var certificateString = NSMutableAttributedString(string: "Código aceito, você ganhou ")
        certificateString.appendAttributedString(numberOfCupsString)
        certificateString.appendAttributedString(NSMutableAttributedString(string: " de cortecia em "))
        certificateString.appendAttributedString(dateOfPurchaseString)
        
        self.certificateAttributedText.attributedText = certificateString
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
