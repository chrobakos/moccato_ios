//
//  PaymentOption.swift
//  Moccato iOS
//
//  Created by Bjarki Sorens on 02/05/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import Foundation

class PaymentOption {
    var id: Int?
    var period: Int?
    var discount: Float?
}