//
//  orderTableViewCell.swift
//  Moccato iOS
//
//  Created by Filip Strycko on 31/05/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import UIKit

class OrderTableViewCell: UITableViewCell {
    
    var orderId:Int?
    var date:NSDate?
    var amountField:String?
    var descriptionField:String?
    
    @IBOutlet weak var topRowLabel: UILabel!
    @IBOutlet weak var bottomRowLabel: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        // Bold arrt
        let blabelFont = UIFont(name: "HelveticaNeue-Bold", size: 15)
        let boldAttr :Dictionary = [NSFontAttributeName : blabelFont!]
        
        // normal attr
        let nlabelFont = UIFont(name: "HelveticaNeue", size: 15)
        let nAttr = [NSFontAttributeName : nlabelFont!]
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd 'of' MMMM"
        let dateString = dateFormatter.stringFromDate(self.date!)
   
        
        // Create attributed string
        var orderIdAttrString = NSAttributedString(string: "#\(orderId!)", attributes:boldAttr)
        var orderDateAttrString = NSAttributedString(string: " - \(dateString)", attributes:nAttr)
        
        var amountAttrString = NSAttributedString(string: amountField!, attributes: boldAttr)
        var descriptionAttrString = NSAttributedString(string: " / \(descriptionField!)", attributes: nAttr)
        
        var topRowAttrString = NSMutableAttributedString(attributedString: orderIdAttrString)
        topRowAttrString.appendAttributedString(orderDateAttrString)
        
        var bottomRowAttrString = NSMutableAttributedString(attributedString: amountAttrString)
        bottomRowAttrString.appendAttributedString(descriptionAttrString)
        
        topRowLabel.attributedText = topRowAttrString
        bottomRowLabel.attributedText = bottomRowAttrString
        
        
    
    }
    
}
