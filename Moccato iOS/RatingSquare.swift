//
//  RatingSquare.swift
//  Moccato iOS
//
//  Created by Filip Strycko on 06/05/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import UIKit

class RatingSquare: UIView {
    
    private var rating:UILabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    private func setup() {
        self.backgroundColor = UIColor(white: 80/255, alpha: 1)
        self.layer.cornerRadius = 5
        self.layer.masksToBounds = true
        rating = UILabel(frame: CGRectMake(0, 0, self.frame.width, self.frame.height))
        rating.textAlignment = NSTextAlignment.Center
        let font = UIFont(name: "HelveticaNeue-Light", size: 15)
        rating.font = font
        rating.textColor = UIColor.whiteColor()
        
        self.addSubview(rating)
    }
    
    internal func setRating(rating:Float){
        self.rating.text = String(format: "%.1f", rating)
    }
}
