//
//  MoccatoServer.swift
//  Moccato iOS
//
//  Created by Filip Strycko on 07/03/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import Foundation
import ReactiveCocoa


class MoccatoServer : ServerApi{
    
    /**
    Defines server requests with particular *path*.
    
    Example:
    
    `Requests.getUser(9).path()`
    */
    enum Requests{
        
        /// Server request
        case verifyToken
        case getUser(Int)
        case auth
        case register
        
        
        /**
        
        :returns: String of path of particular request.
        
        */
        func path() -> String
        {
            switch self{
            case .verifyToken:
                return "token/"
            case .getUser(let id):
                return "users/\(id)"
            case .register:
                return "user/'"
            case .auth:
                return "auth/"
            default:
                return ""
            }
        }
    }
    
    
    override init(){
        super.init();
        
        //define base URL of server
        self.baseUrl = Factory.getBackendURL()!
        
    }


    internal override func setupCustomHeaders() -> [String : String]? {
        if let accessToken = Authentication.getToken(){
            let accessToken = "\(accessToken):"
            let utf8data = accessToken.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
            if let base64Encoded = utf8data?.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
            {
                let toHeader = "Basic \(base64Encoded)"
                return ["Authorization": toHeader]
            }else{ return nil }

        }
        
        return nil
    }
    
    
}