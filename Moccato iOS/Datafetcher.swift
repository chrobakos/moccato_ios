//
//  Datafetcher.swift
//  Moccato iOS
//
//  Created by Bjarki Sorens on 25/02/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import Alamofire
import SwiftyJSON
import ReactiveCocoa

@objc(Datafetcher)
class Datafetcher: NSObject, BackendService {
    
    let app = UIApplication.sharedApplication()
    
    func getCafeterias(callback: (cafeterias: [Cafeteria]?) -> ()) {
        println("WARNING: not implemented")
        callback(cafeterias: nil)
    }
    
    func getFlavors(callback: (flavors: [Flavor]?) -> ()) {
        println("WARNING: not implemented")
        callback(flavors: nil)
    }
    
    func getPackages(callback: (packages: [Package]?) -> ()) {
        println("WARNING: not implemented")
        callback(packages: nil)
    }
    
    func getSubscription(subscriptionId: Int, callback: (subscription: Subscription?) -> ()) {
        println("WARNING: not implemented")
        callback(subscription: nil)
    }
    
    func getSubscriptionTypes(callback: (subscriptionTypes: [SubscriptionType]?) -> ()) {
        println("WARNING: not implemented")
        callback(subscriptionTypes: nil)
    }
    
    func verifyToken(token:String, callback: (tokenVerified: Bool) -> ()) {
        // create the url
        var url = Factory.getValidationBackendURL()! + "token/"
        // show network indicator
        self.app.networkActivityIndicatorVisible = true
        // do the post
        request(.POST, url, parameters: ["token": token], encoding: .JSON)
            .validate()
            .responseJSON {
                (request, response, JSON, error) in
                    self.app.networkActivityIndicatorVisible = false
                    callback(tokenVerified: error != nil) // this is a bug, this should say error == nil
            }
    }
    
    func loginUserWithEmail(email: String, password: String) -> RACSignal {
//        self.app.networkActivityIndicatorVisible = true
//        self.app.networkActivityIndicatorVisible = false
  
        //create login signal
        let loginSignal = MoccatoServer()._post(MoccatoServer.Requests.auth.path(), params: ["email": email, "password": password])
        
        return loginSignal
        
    }
    
    func getUsersSubscriptions() -> RACSignal{
        
        let getUsersSubscriptionsSignal = RACSignal.empty()
        
        return getUsersSubscriptionsSignal
    }
    
    
    func getUser(user_id: Int) -> RACSignal {
        
        let getUserSignal = MoccatoServer()._get(MoccatoServer.Requests.getUser(user_id).path())
        
        return getUserSignal
        
    }
    
    func registerNewUser(name: String, surname: String, email: String, password: String) -> RACSignal {
        let registerUserSignal = MoccatoServer()._post(MoccatoServer.Requests.register.path(), params:[
            "name": name,
            "surname": surname,
            "email": email,
            "password": password //PASSWORD HASH
            ])
        
        return registerUserSignal
    }
    
    func getPaymentAndOrderHistoryOfUser(user_id: Int) -> RACSignal {
        return RACSignal.empty()
    }
    
    func purchaseCoffeeCups(numberOfCups: Int, callback: (purchaseComplete: Bool) -> ()) {
        println("WARNING: Not implemented")
        callback(purchaseComplete: false)
    }
    
    func rateAndReviewCafeteria(cafeteriaId: Int, review: String, rating: Float, callback: (success: Bool) -> ()) {
        println("WARNING: Not implemented")
        callback(success: false)
    }
    
}