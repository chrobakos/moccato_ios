//
//  Address.swift
//  Moccato iOS
//
//  Created by Filip Strycko on 25/04/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import Foundation
import CoreData

@objc(Address)
public class Address: NSManagedObject {

    @NSManaged var id: NSNumber
    @NSManaged var name: String
    @NSManaged var zip_code: String
    @NSManaged var street: String
    @NSManaged var city: String
    @NSManaged var state: String
    @NSManaged var country_code: NSNumber
    @NSManaged var owner_id: NSNumber
    @NSManaged var owner_type: NSNumber
    @NSManaged var created_at: NSDate
    @NSManaged var updated_at: NSDate
    @NSManaged var cafeteria: Cafeteria

}
