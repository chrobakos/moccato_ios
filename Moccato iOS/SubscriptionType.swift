//
//  Subscription.swift
//  Moccato iOS
//
//  Created by Bjarki Sorens on 02/05/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import Foundation

class SubscriptionType {
    var id: Int?
    var name: String?
    var description: String?
    var prices: [Price]?
    var packages: [Package]?
    var trial: Trial?
    var trialId: Int?
    var paymentOptions: [PaymentOption]?
}