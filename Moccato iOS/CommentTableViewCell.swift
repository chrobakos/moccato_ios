//
//  CommentTableViewCell.swift
//  Moccato iOS
//
//  Created by Bjarki Sorens on 22/04/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var commentText: UITextView!
    @IBOutlet weak var commentProfileImage: UIImageView!
    @IBOutlet weak var commentName: UILabel!
    @IBOutlet weak var commentRating: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
