//
//  Package.swift
//  Moccato iOS
//
//  Created by Bjarki Sorens on 02/05/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import Foundation

class Package {
    var id: Int?
    var name: String?
    var description: String?
    var quantity: Int?
    var updatedAt: NSDate?
    var createdAt: NSDate?
}