//
//  CafeteriaScore.swift
//  Moccato iOS
//
//  Created by Filip Strycko on 25/04/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import Foundation
import CoreData

@objc(CafeteriaScore)
public class CafeteriaScore: NSManagedObject {

    @NSManaged var id: NSNumber
    @NSManaged var score: NSNumber
    @NSManaged var text: String
    @NSManaged var created_at: NSDate
    @NSManaged var user: User
    @NSManaged var cafeteria: Cafeteria

}
