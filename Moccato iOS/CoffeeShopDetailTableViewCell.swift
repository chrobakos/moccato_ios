//
//  CoffeeShopDetailTableViewCell.swift
//  Moccato iOS
//
//  Created by Bjarki Sorens on 22/04/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import UIKit
import MapKit

class CoffeeShopDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var detailAddress1: UILabel!
    @IBOutlet weak var detailAddress2: UILabel!
    @IBOutlet weak var detailDistanceIndicator: UILabel!
    @IBOutlet weak var detailMap: MKMapView!
    @IBOutlet weak var detailCategory: UILabel!
    @IBOutlet weak var detailHoursIndicator: UILabel!
    @IBOutlet weak var detailOpeningHours: UITextView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
