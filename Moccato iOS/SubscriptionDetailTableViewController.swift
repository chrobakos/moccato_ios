//
//  SubscriptionDetailTableViewController.swift
//  Moccato iOS
//
//  Created by Filip Strycko on 20/05/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import UIKit

class SubscriptionDetailTableViewController: UITableViewController {

    @IBOutlet weak var subscriptionCell: SubscriptionCell!
    var subscription:Subscription? = nil
    

    override func viewDidLoad() {
        
    }
    
    override func viewWillAppear(animated: Bool) {
        self.tableView.tableHeaderView = UIView(frame: CGRectMake(0, 0, self.view.frame.size.width, 0))
        if let subscription = self.subscription{
            subscriptionCell.subscriptionName = subscription.package!.name!
            subscriptionCell.subscriptionDescription = "\(subscription.package!.quantity!) capsuls/month \n\(subscription.package!.description!) \nExpiration date 22/22/2222"
        }
        
        
    }


    
}
