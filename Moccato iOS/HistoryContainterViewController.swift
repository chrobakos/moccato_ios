//
//  HistoryContainterViewController.swift
//  Moccato iOS
//
//  Created by Filip Strycko on 31/05/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import UIKit
import SwiftyJSON
import ReactiveCocoa

class HistoryContainterViewController: UITableViewController {
    
    let dataManager = Datamanager(managedObjectContext: Factory.getManagedObjectContext())
    
    var shippingItems:[Order] = []
    var paymentItems:[Order] = []
    var nextPayments:[Order] = []
    
    var currentDataSource:Int = 0
    var segmentationControl:UISegmentedControl = UISegmentedControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //pull to refresh
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.backgroundColor = StyleUtilities.moccatoVeryLightGreyColor
        self.refreshControl!.tintColor = StyleUtilities.moccatoNavigationBarTintColor
        self.refreshControl!.addTarget(self, action: "pulledToRefresh:", forControlEvents: UIControlEvents.ValueChanged)
        
        let tableHeaderView = UIView(frame: CGRectMake(0, 0, self.view.frame.width, 50))
        segmentationControl = UISegmentedControl(frame: CGRectMake(10, 5, self.view.frame.width-20, 40))
        segmentationControl.tintColor = StyleUtilities.moccatoNavigationBarTintColor
        segmentationControl.insertSegmentWithTitle("Orders", atIndex: 0, animated: false)
        segmentationControl.insertSegmentWithTitle("Payments", atIndex: 1, animated: false)
        segmentationControl.selectedSegmentIndex = 0
        segmentationControl.addTarget(self, action: "dataSourceChanged:", forControlEvents: UIControlEvents.ValueChanged)
        
        tableHeaderView.addSubview(segmentationControl)
        self.tableView.tableHeaderView = tableHeaderView
        
        
        var hud = Loading.showLoadingInView(self.view, withText: "Loading data...")
        self.loadDataFromBackendService().subscribeCompleted { () -> Void in
            hud.hide(true)
        }
    }

    func loadDataFromBackendService() -> RACSignal{
        
        //load payments
        var loginSignal = RACSignal.empty()
        if let user = dataManager.fetchCurrentUserData(){
            loginSignal = Factory.getBackendService().getPaymentAndOrderHistoryOfUser(user.id.integerValue)
            loginSignal.deliverOnMainThread().subscribeNext({ (responseDict) -> Void in
                
                self.shippingItems.removeAll(keepCapacity: false)
                self.paymentItems.removeAll(keepCapacity: false)
                self.nextPayments.removeAll(keepCapacity: false)
                
                
                let response = JSON(responseDict)
                
                for item in response["shipping"].arrayValue {
                    let o = Order()
                    o.id = item["id"].intValue
                    o.date = NSDate(timeIntervalSince1970: item["date"].doubleValue)
                    o.amount = item["amount"].stringValue
                    o.description = item["description"].stringValue
                    
                    self.shippingItems.append(o)
                }
                
                for item in response["payments"].arrayValue {
                    let o = Order()
                    o.id = item["id"].intValue
                    o.date = NSDate(timeIntervalSince1970: item["date"].doubleValue)
                    o.amount = item["amount"].stringValue
                    o.description = item["description"].stringValue
                    
                    self.paymentItems.append(o)
                }
                
                for item in response["nextPayments"].arrayValue {
                    let o = Order()
                    o.id = item["id"].intValue
                    
                    o.date = NSDate(timeIntervalSince1970: item["date"].doubleValue)
                    o.amount = item["amount"].stringValue
                    o.description = item["description"].stringValue
                    
                    self.nextPayments.append(o)
                }
                self.tableView.reloadData()
                //hud.hide(true)
                
                }, error: { (error) -> Void in
                    
                    //Loading.showError(self.view, withText: "Not possible to load data.")
                    
            })
            
            
            
        }
        
        return loginSignal
    }
    
    
    func pulledToRefresh(sender:AnyObject){
        
        self.loadDataFromBackendService().deliverOnMainThread().subscribeCompleted { () -> Void in
            self.refreshControl?.endRefreshing()
        }
        
    }
    
    func delay(delay:Double, closure:()->()) {
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }
    
    
    func dataSourceChanged(sender:AnyObject) {
        currentDataSource = self.segmentationControl.selectedSegmentIndex
        self.tableView.reloadData()
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(currentDataSource == 0){
            return shippingItems.count
        }else{
            return paymentItems.count + nextPayments.count
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {

        if(currentDataSource == 0){
            //if shipping
            return 70
        }else{
            //if payments
            if(isNextPayment(indexPath.row)){
                //if subscription cell
                return 125
            }else{
                return 70
            }
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if(currentDataSource == 0){
            //if shipping
            let shippingOrder = self.shippingItems[indexPath.row]
            let cell = self.tableView.dequeueReusableCellWithIdentifier("orderCell") as! OrderTableViewCell
            cell.orderId = shippingOrder.id
            cell.date = shippingOrder.date
            cell.amountField = shippingOrder.amount
            cell.descriptionField = shippingOrder.description
            
            return cell
        }else{
            //if payments
            if(isNextPayment(indexPath.row)){
                //if subscription cell (next payments)
                let nextPaymentItem = self.nextPayments[indexPath.row]
                let cell = self.tableView.dequeueReusableCellWithIdentifier("subscriptionCell") as! SubscriptionCell
                cell.subscriptionId = nextPaymentItem.id!
                cell.subscriptionName = "Next payment"
                
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "dd 'of' MMMM YYYY"
                let dateString = dateFormatter.stringFromDate(nextPaymentItem.date!)
                
                
                cell.subscriptionDescription = "\(dateString)\n\(nextPaymentItem.amount!)\n\(nextPaymentItem.description!)"
                
                return cell
            }else{
                //if normal payment
                let paymentItem = self.paymentItems[self.toPaymentRow(indexPath.row)]
                let cell = self.tableView.dequeueReusableCellWithIdentifier("orderCell") as! OrderTableViewCell
                cell.orderId = paymentItem.id
                cell.date = paymentItem.date
                cell.amountField = paymentItem.amount
                cell.descriptionField = paymentItem.description
                
                return cell
            }
        }
        
    }
    
    
    
    private func isNextPayment(row:Int) -> Bool{
        return nextPayments.count-row > 0
    }
    
    private func toPaymentRow(row:Int) -> Int{
        return row - nextPayments.count
    }
}
