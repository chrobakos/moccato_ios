//
//  RateReviewViewController.swift
//  Moccato iOS
//
//  Created by Bjarki Sorens on 07/06/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import UIKit

class RateReviewViewController: UIViewController, UIBarPositioningDelegate, UINavigationBarDelegate, UITextViewDelegate {

    // set by parent controller
    var cafeteria: Cafeteria?

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var reviewTextView: UITextView!
    
    let dataManager = Datamanager(managedObjectContext: Factory.getManagedObjectContext())
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navBar.delegate = self
        self.reviewTextView.delegate = self
        
        self.setupConstraintsForScrollView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func cancelButtonPushed(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func sendButtonPushed(sender: UIBarButtonItem) {
        Factory.getBackendService().rateAndReviewCafeteria(Int(self.cafeteria!.id), review: self.reviewTextView.text, rating: 4.5, callback: {
            (success: Bool) in
            self.dataManager.saveReview(self.reviewTextView.text, rating: 4.5, forCafeteria: self.cafeteria!)
            NSNotificationCenter.defaultCenter().postNotificationName("modalDismissed", object: nil)
            self.dismissViewControllerAnimated(true, completion: nil)
        })
    }
    
    // MARK: - Delegates
    
    func positionForBar(bar: UIBarPositioning) -> UIBarPosition {
        return UIBarPosition.TopAttached
    }
    
    // MARK: - Private functions
    
    func setupConstraintsForScrollView() {
        
        var leftConstraint = NSLayoutConstraint(
            item: self.contentView,
            attribute: .Leading,
            relatedBy: .Equal,
            toItem: self.view,
            attribute: .Left,
            multiplier: 1.0,
            constant: 0)
            
        self.view.addConstraint(leftConstraint)
        
        var rightConstraint = NSLayoutConstraint(
            item: self.contentView,
            attribute: .Trailing,
            relatedBy: .Equal,
            toItem: self.view,
            attribute: .Right,
            multiplier: 1.0,
            constant: 0)
        
        self.view.addConstraint(rightConstraint)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
