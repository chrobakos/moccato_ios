//
//  DatafetcherServiceStub.swift
//  Moccato iOS
//
//  Created by Bjarki Sorens on 01/05/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import Foundation
import CoreData
import UIKit
import ReactiveCocoa

@objc(DatafetcherServiceStub)
class DatafetcherServiceStub: NSObject, BackendService {
    
    func delay(delay:Double, closure:()->()) {
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }
    
    func getCafeterias(callback: (cafeterias: [Cafeteria]?) -> ()) {
        var results:[Cafeteria] = []
        
        let names = [
            "Starbucks Kastrup",
            "Barking Barista",
            "El Cafe Loco",
            "Sorte Skummemælken"
        ]
        let descs = [
            "Fresh coffee at the Airport",
            "The most dog-friendly cafeteria in town!",
            "We might be loco, but you'll tip us before you gogo",
            "Den sorteste cup of joe du kan finde"
        ]
        let streets = [
            "Kastrupvej",
            "Norre Alle 53",
            "Stroget 25",
            "Istedgade 1"
        ]

        for i in 0...3 {
            let cdShop = NSEntityDescription.insertNewObjectForEntityForName("Cafeteria", inManagedObjectContext: Factory.getManagedObjectContext())
                as! Cafeteria
            
            let caf_address = NSEntityDescription.insertNewObjectForEntityForName("Address", inManagedObjectContext: Factory.getManagedObjectContext())
                as! Address
            
            cdShop.id = i
            cdShop.name = names[i]
            cdShop.desc = descs[i]
            cdShop.latitude = i*300
            cdShop.longitude = i*130
            
            caf_address.zip_code = String(i+100)
            caf_address.street = streets[i]
            caf_address.city = "Copenhagen"
            cdShop.address = caf_address
            
            results.append(cdShop)
        }
        
        // core data population done
        callback(cafeterias: results)
    }
    
    func getFlavors(callback: (flavors: [Flavor]?) -> ()) {
        var results:[Flavor] = []
        
        var flavor1 = Flavor()
        flavor1.id = 1
        flavor1.name = "Ipanema"
        flavor1.description = "Description of  the Ipanama Flavor"
        flavor1.active = true
        results.append(flavor1)
        
        var flavor2 = Flavor()
        flavor2.id = 2
        flavor2.name = "Copacabana"
        flavor2.description = "Description of the Copacabana Flavor"
        flavor2.active = false
        results.append(flavor2)
        
        callback(flavors: results)
    }
    
    func getPackages(callback: (packages: [Package]?) -> ()) {
        var results: [Package] = []
        
        var package1 = Package()
        package1.id = 1
        package1.name = "Uber Coffee Package"
        package1.description = "Package with 90 pods of the best coffee ever."
        package1.updatedAt = NSDate()
        package1.createdAt = NSDate()
        results.append(package1)
        
        var package2 = Package()
        package2.id = 2
        package2.name = "Hyper Coffee Package"
        package2.description = "Package with 90 pods of the best coffee ever."
        package2.updatedAt = NSDate()
        package2.createdAt = NSDate()
        results.append(package2)
        
        callback(packages: results)
    }
    
    func getSubscription(subscriptionId: Int, callback: (subscription: Subscription?) -> ()) {
        var subscription = Subscription()
        
        var user = User()
        user.email = "user@moccato.com.br"
        
        var package = Package()
        package.name = "Super Package"
        package.description = "Package description."
        package.quantity = 60
        
        subscription.id = 3
        subscription.user = user
        subscription.package = package
        subscription.updatedAt = NSDate()
        subscription.createdAt = NSDate()
        
        callback(subscription: subscription)
    }
    
    func getSubscriptionTypes(callback: (subscriptionTypes: [SubscriptionType]?) -> ()) {
        var results: [SubscriptionType] = []
        var subscriptionType = SubscriptionType()
        
        var price = Price()
        price.id = 1
        price.packId = 1
        price.fullPrice = 15000
        price.podPrice = 1000
        
        var package = Package()
        package.id = 1
        package.name = "Uber Coffee Package"
        package.description = "Package with 90 pods of the best coffee ever."
        package.quantity = 90
        
        var trial = Trial()
        trial.id = 1
        trial.months = 1
        trial.discount = 100
        
        var paymentOption = PaymentOption()
        paymentOption.id = 1
        paymentOption.period = 1
        paymentOption.discount = 1
        
        subscriptionType.id = 1
        subscriptionType.name = "Assinatura Tipo 1"
        subscriptionType.description = "Bla bla bla"
        subscriptionType.prices = [price]
        subscriptionType.packages = [package]
        subscriptionType.trial = trial
        subscriptionType.trialId = 1
        subscriptionType.paymentOptions = [paymentOption]
        
        callback(subscriptionTypes: [subscriptionType, subscriptionType])
    }
    
    func verifyToken(token: String, callback: (tokenVerified: Bool) -> ()) {
        callback(tokenVerified: true)
    }
    
    func loginUserWithEmail(email: String, password: String) ->RACSignal {
        
        let mockup = RACReplaySubject()
        mockup.sendNext([
            "token": "mockuptoken",
            "user_id": "9"
            ])
        
        mockup.sendCompleted()
        return mockup
    }
    
    func getUser(user_id: Int) -> RACSignal {
        let mockup = RACReplaySubject()
        
        mockup.sendNext([
            "email": "mockup@email.com.br",
            "first_name": "John",
            "last_name": "Doe"
            ])
        
        mockup.sendCompleted()
        return mockup
    }
    
    func getUsersSubscriptions() -> RACSignal {
        let mockup = RACReplaySubject()
        
        
        mockup.sendNext([[
            "id":123,
            "user":["id":9],
            "expiration":NSDate(timeIntervalSinceNow: 1000),
            "package":[
                "id":234,
                "name":"Moccato Trenta",
                "description":"R$ 90,00 (2 free coffees)",
                "quantity":666,
                "updatedAt":NSDate(timeIntervalSinceNow: 0),
                "createdAt":NSDate(timeIntervalSinceNow: 0)
            ],
            "updatedAt":NSDate(timeIntervalSinceNow: 0),
            "createdAt":NSDate(timeIntervalSinceNow: 0)
            ],
            [
                "id":345,
                "user":["id":9],
                "expiration":NSDate(timeIntervalSinceNow: 10300),
                "package":[
                    "id":264,
                    "name":"Moccato Venti",
                    "description":"R$ 70,00 (1 free coffees)",
                    "quantity":667,
                    "updatedAt":NSDate(timeIntervalSinceNow: 0),
                    "createdAt":NSDate(timeIntervalSinceNow: 0)
                ],
                "updatedAt":NSDate(timeIntervalSinceNow: 0),
                "createdAt":NSDate(timeIntervalSinceNow: 0)
            ]
            ])
        
        
        mockup.sendCompleted()
        return mockup
    }
    
    func registerNewUser(name: String, surname: String, email: String, password: String) -> RACSignal {
        return RACSignal.empty()
    }
    
    func generateOrder() -> Order{
        let o = Order()
        o.id = Int(arc4random_uniform(5000))+4999
        o.amount = "\(Int(arc4random_uniform(50))+39)$/50 capsules"
        o.date = NSDate(timeIntervalSince1970:Double(arc4random_uniform(32000000))+1426940622)
        o.description = "Rio de Janeiro, RJ"
        return o
    }
    
    func getPaymentAndOrderHistoryOfUser(user_id:Int) -> RACSignal
    {
        
        let mockup = RACReplaySubject()
        var shipping:[[String:AnyObject]] = []
        var payments:[[String:AnyObject]] = []
        var nextPayments:[[String:AnyObject]] = []
        for var i = 0; i < 10; i++
        {
            let shippingItem = self.generateOrder()
            shipping.append(shippingItem.toDictionary())
            
            let payment = self.generateOrder()
            payments.append(payment.toDictionary())
            
        }
        
        nextPayments = [self.generateOrder().toDictionary(),self.generateOrder().toDictionary()]
        
        self.delay(1, closure: { () -> () in
            mockup.sendNext([
                "shipping":shipping,
                "payments":payments,
                "nextPayments":nextPayments
                ])
            mockup.sendCompleted()
        })
        
        return mockup
    }
    
    func purchaseCoffeeCups(numberOfCups: Int, callback: (purchaseComplete: Bool) -> ()) {
        callback(purchaseComplete: true)
    }
    
    func rateAndReviewCafeteria(cafeteriaId: Int, review: String, rating: Float, callback: (success: Bool) -> ()) {
        callback(success: true)
    }
    
}
