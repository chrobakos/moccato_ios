//
//  ProfileViewController.swift
//  Moccato iOS
//
//  Created by Bjarki Sorens on 19/04/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import UIKit
import SwiftyJSON
import ReactiveCocoa

class ProfileViewController: UITableViewController, UIActionSheetDelegate {

    var loginLogOutDelegate:LoginLogoutDelegate?
    var subscriptions:[Subscription] = []
    let bottomCells:[String] = ["Free coffee", "My free", "Flavors", "History"]

    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        // Do any additional setup after loading the view.
        self.loginLogOutDelegate = UIApplication.sharedApplication().delegate as? LoginLogoutDelegate
        
        Factory.getBackendService().getUsersSubscriptions().subscribeNext({ (responseDict) -> Void in
            
            let subscriptions = JSON(responseDict)
            
            // IDEA : use MAP:
            var newSubscriptions = [Subscription]()
            var newIndexPaths = [NSIndexPath]()
            var row:Int = 1
            for subscr in subscriptions.arrayValue {
                
                
                let indexPath = NSIndexPath(forRow: row, inSection: 0)
                row++
                
                var newSubscr = Subscription()
                newSubscr.id = subscr["id"].intValue
                newSubscr.user = nil
                //                newSubscr.expiration = subscr["expiration"]
                var newPackage = Package()
                newPackage.id = subscr["package"]["id"].intValue
                newPackage.name = subscr["package"]["name"].stringValue
                newPackage.description = subscr["package"]["description"].stringValue
                newPackage.quantity = subscr["package"]["quantity"].intValue
                
                newSubscr.package = newPackage
                
                newSubscriptions.append(newSubscr)
                newIndexPaths.append(indexPath)
            }
            
            self.tableView.beginUpdates()
            self.tableView.insertRowsAtIndexPaths(newIndexPaths, withRowAnimation: UITableViewRowAnimation.Automatic)
            self.subscriptions += newSubscriptions
            self.tableView.endUpdates()
            
            
        }, error: { (error) -> Void in
            Loading.showError(self.view, withText: "Error loading user subscriptions")
        })
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    @IBAction func logOutButtonPushed(sender: AnyObject) {
        var sheet = UIActionSheet(
            title: "Are you sure you want to log out?",
            delegate: self,
            cancelButtonTitle: "No",
            destructiveButtonTitle: "Yes, I'm sure")
    
        sheet.showInView(self.view)
    }
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == actionSheet.destructiveButtonIndex {
            Authentication.clearToken()
            self.loginLogOutDelegate?.userDidFinishLogOut()
        }
    }

    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1 + self.subscriptions.count + self.bottomCells.count;
// user cell --^                                ^-- last bottom cells
//                       ^-- subscription cells
    }
    
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        if(indexPath.row == 0){
            return 90
        }else if(self.subscriptions.count - indexPath.row >= 0 ){
            return 125
        }else{
            return 45
        }
        
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
       
        if(isProfileRow(indexPath.row)){
            
            let cell = self.tableView.dequeueReusableCellWithIdentifier("userAccountCell") as! UserAccountCell
            cell.userNameLabel.text = "Don Simon"
            cell.userEmailLabel.text = "contact@donsimon.com"
            
            return cell
            
        }else if(isSubscriptionRow(indexPath.row)){
            let subscription = self.subscriptions[toSubscriptionRow(indexPath.row)]
            
            let cell = self.tableView.dequeueReusableCellWithIdentifier("subscriptionCell") as! SubscriptionCell
            cell.subscriptionName = subscription.package!.name!
            cell.subscriptionDescription = "\(subscription.package!.quantity!) capsuls/month \n\(subscription.package!.description!) \nExpiration date 22/22/2222"
            
            return cell
            
        }else{
            let bottomCellRowId = toBottomCellRow(indexPath.row)
            let cell = self.tableView.dequeueReusableCellWithIdentifier("simpleCell") as! UITableViewCell
            cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
            cell.textLabel?.text = self.bottomCells[bottomCellRowId]
            return cell
            
        }
        

        
    }
    

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        

        if(isProfileRow(indexPath.row)){        //if its user profile cell
                        println("profileRow")
            self.performSegueWithIdentifier("userProfileSegue", sender: self)
        }else if(isSubscriptionRow(indexPath.row)){    // if subscription detail
                        println("subscription Row")
            self.performSegueWithIdentifier("subscriptionDetailsSegue", sender: self)
        }else{
            let bottomCellRowId = toBottomCellRow(indexPath.row)
            
            //Warning if not enough elements in bottomCells array
            if(self.bottomCells.count < bottomCellRowId+1 ){
                println("ERROR: Selected bottom cell is not in the bottomCells array")
                return;
            }
            
            if(bottomCellRowId == 0){
                //Free coffee
            }else if(bottomCellRowId == 1){
                //My free
            }else if(bottomCellRowId == 2){
                //Flavors
            }else if(bottomCellRowId == 3){
                //History
                self.performSegueWithIdentifier("historySegue", sender: self)
            }
        }
        
        
        
    }
    

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {

        let indexPath = tableView.indexPathForSelectedRow()
        
        self.tableView.deselectRowAtIndexPath(indexPath!, animated: true)
        
        if(isProfileRow(indexPath!.row)){
            //for user profile
        }else if(isSubscriptionRow(indexPath!.row)){
            let destinationVC:SubscriptionDetailTableViewController = segue.destinationViewController as! SubscriptionDetailTableViewController
            let subscriptionRowId = toSubscriptionRow(indexPath!.row)
            let subscription = self.subscriptions[subscriptionRowId]
            destinationVC.subscription = subscription
        }
        
        
    }
    
    
    //MARK: Row number helpers
    
    /// Is row the profile row (very first one) ?
    private func isProfileRow(row:Int) -> Bool {
        return row == 0
    }
    
    /// Is row one of the subscription rows?
    private func isSubscriptionRow(row:Int) -> Bool {
        return (self.subscriptions.count - row >= 0)
    }
    
    /// Transform indexPath.row into the row inside of self.subscriptions array
    private func toSubscriptionRow(row:Int) -> Int {
        return  self.subscriptions.count - row
    }
    
    /// Transform indexPath.row into the row inside of self.bottomCells array
    private func toBottomCellRow(row:Int) -> Int {
        return row - self.subscriptions.count - 1
    }
}
