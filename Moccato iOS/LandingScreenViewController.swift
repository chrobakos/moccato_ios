//
//  LandingScreenViewController.swift
//  Moccato iOS
//
//  Created by Bjarki Sorens on 16/04/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import UIKit

class LandingScreenViewController: UIViewController, UIPageViewControllerDataSource {

    var pageViewController: UIPageViewController?
    var images = [
        "page1.png",
        "page2.png",
        "page3.png",
        "page4.png"
    ]
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        var pageControlAppearance = UIPageControl.appearance()
        pageControlAppearance.pageIndicatorTintColor = UIColor.lightGrayColor()
        pageControlAppearance.currentPageIndicatorTintColor = UIColor.blackColor()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.pageViewController = self.storyboard?.instantiateViewControllerWithIdentifier("PageViewController") as? UIPageViewController
        self.pageViewController?.dataSource = self
        
        var startingLandingPageViewController = self.viewControllerAtIndex(0)
        self.pageViewController!.setViewControllers([startingLandingPageViewController!], direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil)
        
        self.pageViewController?.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 45)
        self.addChildViewController(self.pageViewController!)
        self.view.addSubview(self.pageViewController!.view)
        self.pageViewController?.didMoveToParentViewController(self)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        var pageController = viewController as! LandingPageViewController
        var index = pageController.currentPageIndex
        index = index! - 1
        return self.viewControllerAtIndex(index!)
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        var pageController = viewController as! LandingPageViewController
        var index = pageController.currentPageIndex
        index = index! + 1
        return self.viewControllerAtIndex(index!)
    }
        
    private func viewControllerAtIndex(index: NSInteger) -> UIViewController? {
        if index < 0 || index >= self.images.count {
            return nil
        }
        
        var landingPageViewController = self.storyboard?.instantiateViewControllerWithIdentifier("LandingPageViewController") as! LandingPageViewController
        landingPageViewController.currentImageFile = self.images[index]
        landingPageViewController.currentPageIndex = index
        
        return landingPageViewController
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return self.images.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    
    // MARK: - Navigation
    
    
    @IBAction func loginButtonPushed(sender: UIButton) {
        self.performSegueWithIdentifier("loginSegue", sender: self)
    }
    @IBAction func registrationButtonPushed(sender: UIButton) {
        self.performSegueWithIdentifier("registrationSegue", sender: self)
    }

    
    /*
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    

}
