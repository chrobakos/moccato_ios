//
//  CafeteriasViewController.swift
//  Moccato iOS
//
//  Created by Bjarki Sorens on 25/02/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import UIKit
import CoreData
import ReactiveCocoa
import MapKit
import CoreLocation

class CafeteriasViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var showFavouritedButton: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mapView: MKMapView!
    
    
    var showFavourited:Bool = false
    var searchBar:UISearchBar = UISearchBar()
    var shops:[Cafeteria] = []
    var favourited:[Cafeteria] = []
    let locationManager = CLLocationManager()
    var currentUser:User? = nil
    let dataManager = Datamanager(managedObjectContext: Factory.getManagedObjectContext())
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        //fetch favourited by current user
        self.favourited = self.dataManager.fetchFavourited()
        self.currentUser = self.dataManager.fetchCurrentUserData()
        self.tableView.reloadData()
        
        self.locationManager.requestWhenInUseAuthorization()
        
        //scroll tableView a bit, so searchBar is not visible
        self.tableView.setContentOffset(CGPointMake(0, 50), animated: false)

    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.searchBar.delegate = self
        self.currentUser = self.dataManager.fetchCurrentUserData()
        self.shops = self.dataManager.fetchCafeterias()

        
        
        // Initialize search bar and put it into tableView
        self.searchBar = UISearchBar(frame: CGRectMake(0, 0, self.view.frame.width, 50))
        self.searchBar.delegate = self;
        self.searchBar.placeholder = "Search"
        self.searchBar.searchBarStyle = UISearchBarStyle.Minimal
        self.tableView.tableHeaderView = self.searchBar;
        
        
        if self.shops.isEmpty { // need to call API
            Factory.getBackendService().getCafeterias({
                (cafeterias: [Cafeteria]?) in
                self.shops = self.dataManager.fetchCafeterias()
                self.tableView.reloadData()
            })
        }
        
        
        // Can this be somewhere else ? , filip: nope
        self.showFavouritedButton.rac_command = RACCommand(signalBlock: { (sender) -> RACSignal! in
            self.showFavourited = self.showFavourited ? false : true
            self.tableView.reloadData()
            if self.showFavourited {
                self.title = "Favourited"
            } else {
                self.title = "Moccato"
            }
            return RACSignal.empty()
        })
        
    }
    
    // MARK: - Table protocols
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var shopInfo:Cafeteria
        
        if self.showFavourited {
            shopInfo = self.favourited[indexPath.row]
        } else {
            shopInfo = self.shops[indexPath.row]
        }

        
        var cell = self.tableView.dequeueReusableCellWithIdentifier("CoffeeShopCell") as! CoffeeShopTableViewCell
        let address:Address = shopInfo.address

        cell.cafeteriaName.text = shopInfo.name
        cell.cafeteriaAddress.text = address.street+"\n"+address.city
        cell.cafeteriaDistance.text = "200 metros"
        cell.ratingSquare.setRating(4.6)
        
        // If user is logged in
        if let currentUser = self.currentUser {
            
            if shopInfo.favoritedBy.containsObject(currentUser){
                cell.cafeteriaBookmarkImage.hidden = false
            }else{
                cell.cafeteriaBookmarkImage.hidden = true
            }
        }else{
            cell.cafeteriaBookmarkImage.hidden = true
        }
        
        
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.showFavourited){
            return self.favourited.count
        }else{
            return self.shops.count
        }
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    // hides the keyboard
    func scrollViewDidScroll(scrollView: UIScrollView) {
        self.searchBar.resignFirstResponder()
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if(segue.identifier == "cafeteriaDetail"){
            let vc = segue.destinationViewController as! CafeteriaViewController
            let group = (self.showFavourited ? self.favourited : self.shops )
            var shop = group[self.tableView.indexPathForSelectedRow()!.row]
            vc.shop = shop
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: Searchbar delegates
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if count(searchText) == 0 {
            self.shops = self.dataManager.fetchCafeterias()
            self.tableView.reloadData()
            return
        } else if let results = self.dataManager.fetchCafeteriasGivenSearchText(searchText) as [Cafeteria]? {
            self.shops = results
            self.tableView.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }

}
