//
//  Orders.swift
//  Moccato iOS
//
//  Created by Filip Strycko on 31/05/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import Foundation

class Order{
    var id: Int?
    var date: NSDate?
    var amount:String?
    var description: String?
    
    func toDictionary() -> [String:AnyObject]{
        return ["id" :self.id!,
            "date" :self.date!.timeIntervalSince1970,
            "amount" :self.amount!,
            "description" :self.description!]
    }
}