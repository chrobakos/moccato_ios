//
//  CoffeeShopViewController.swift
//  Moccato iOS
//
//  Created by Bjarki Sorens on 13/03/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import UIKit

class CafeteriaViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, CoffeeShopHeaderProtocol, DynamicCellHeight {
    
    var shop:Cafeteria?
    var comments: [CafeteriaScore]?
    var currentView:CoffeeShopViewSegment = .DetailView
    var detailSizingCell: CoffeeShopDetailTableViewCell?
    var commentSizingCell: CommentTableViewCell?
    let dataManager = Datamanager(managedObjectContext: Factory.getManagedObjectContext())
    
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.comments = self.shop!.ratings.allObjects as? [CafeteriaScore]
        
        self.tableView.delegate = self
        self.tableView.dataSource = self

        self.setupTableHeaderView()
        self.setupNavigationItems()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        self.sizeHeader()
    }
    
    // MARK: - Private helpers
    
    private func setupNavigationItems() {
        let ratingSquare = RatingSquare(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        ratingSquare.setRating(4.6)
        var ratingBarButtonView = UIBarButtonItem(customView: ratingSquare)
        
        var favoriteBarButton = UIBarButtonItem(image: UIImage(named: "Bookmark"), style: UIBarButtonItemStyle.Plain, target: self, action: "favoriteButtonClicked")
        
        var rateButton = UIBarButtonItem(image: UIImage(named: "RateReview"), style: UIBarButtonItemStyle.Plain, target: self, action: "rateButtonClicked")
        
        self.navigationItem.rightBarButtonItems = [ratingBarButtonView, favoriteBarButton, rateButton]
    }
    
    private func setupTableHeaderView() {
        var coffeeHeaderView = NSBundle.mainBundle().loadNibNamed("CoffeeShopHeader", owner: self, options: nil).first as? CoffeeShopHeader
        coffeeHeaderView?.delegate = self
        coffeeHeaderView?.cafeteriaTitle.text = self.shop!.name
        coffeeHeaderView?.cafeteriaAddress.text = self.shop!.address.street+"\n"+self.shop!.address.city
        
        self.tableView.tableHeaderView = coffeeHeaderView
    }
    
    private func sizeHeader() {
        var header = self.tableView.tableHeaderView as? CoffeeShopHeader
        header?.frame.size.height = header!.contentView.frame.size.height
        self.tableView.tableHeaderView = header
    }
    
    // MARK: - Events
    
    func favoriteButtonClicked() {
        //get current user
        if let user = self.dataManager.fetchCurrentUserData()
        {
            var users_favourite = user.favoriteCafeterias
            
            //find out if user already has cafeteria among favourites
            var contains:Bool = users_favourite.containsObject(self.shop!)
            if (contains) {
                //if yes, change set to MutableSet, remove object, save
                var users_favourite = NSMutableSet(set: users_favourite)
                users_favourite.removeObject(self.shop!)
                user.favoriteCafeterias = users_favourite
                Loading.showSuccess(self.view, withText: "Removed from favorites", hideAfterDelay: 1)
            } else {
                //if no, change set to MutableSet, add object, save
                var users_favourite = NSMutableSet(set:users_favourite)
                users_favourite.addObject(self.shop!)
                user.favoriteCafeterias = users_favourite
                Loading.showSuccess(self.view, withText: "Added to favorites", hideAfterDelay: 1)
            }
        }
    }
    
    func rateButtonClicked() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "modalDidDismiss", name: "modalDismissed", object: nil)
        self.performSegueWithIdentifier("rateReviewSegue", sender: self)
    }
    
    func modalDidDismiss() {
        self.comments = self.shop!.ratings.allObjects as? [CafeteriaScore]
        self.tableView.reloadData()
    }
    
    // MARK: - Table view protocols
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if self.currentView == .DetailView {
            var cell = self.tableView.dequeueReusableCellWithIdentifier("CoffeeShopDetailViewCell") as! CoffeeShopDetailTableViewCell
            self.configureBasicCell(cell, atIndexPath: indexPath)
            return cell
        } else {
            var cell = self.tableView.dequeueReusableCellWithIdentifier("CoffeeShopCommentViewCell") as! CommentTableViewCell
            self.configureBasicCell(cell, atIndexPath: indexPath)
            return cell
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.currentView == .DetailView {
            return 1
        } else {
            if self.comments == nil {
                return 0
            } else {
                return self.comments!.count
            }
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return self.heightForCellAtIndexPath(indexPath)
    }
    
    func configureBasicCell(cell: UITableViewCell, atIndexPath indexPath: NSIndexPath) {
        if self.currentView == .DetailView {
            let cell = cell as! CoffeeShopDetailTableViewCell
            cell.detailAddress1.text = "Shopping Rio Sul - Pisco G3, Rua Lauro Miller, 116."
            cell.detailAddress2.text = "Botafogo, Rio de Janeiro"
            cell.detailDistanceIndicator.text = "A 200 metros da sua localização atual"
            cell.detailCategory.text = "American"
            cell.detailHoursIndicator.text = "Open now"
            cell.detailOpeningHours.text = "Mon: 10h to 22h \nTue: 10h to 22h \nWed: 10h to 22h\nThu: 10h to\nFri: 10h to 22h"
        } else {
            if let comments = self.comments {
                var score = comments[indexPath.row]
                let cell = cell as! CommentTableViewCell
                
                cell.commentName.text = "Fulano da Dilva Neto"
                cell.commentRating.text = "\(score.score)"
                cell.commentText.text = score.text
            }
        }
    }
    
    func heightForCellAtIndexPath(indexPath: NSIndexPath) -> CGFloat {
        if self.currentView == .DetailView {
            if self.detailSizingCell == nil {
                self.detailSizingCell = self.tableView.dequeueReusableCellWithIdentifier("CoffeeShopDetailViewCell") as? CoffeeShopDetailTableViewCell
            }
            self.configureBasicCell(self.detailSizingCell!, atIndexPath: indexPath)
            self.detailSizingCell?.bounds = CGRect(x: 0, y: 0, width: CGRectGetWidth(self.tableView.bounds), height: CGRectGetHeight(self.detailSizingCell!.bounds))
            self.detailSizingCell?.setNeedsLayout()
            self.detailSizingCell?.layoutIfNeeded()
            
            var size:CGSize = self.detailSizingCell!.contentView.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize)
            return size.height + 1
        } else {
            if self.commentSizingCell == nil {
                self.commentSizingCell = self.tableView.dequeueReusableCellWithIdentifier("CoffeeShopCommentViewCell") as? CommentTableViewCell
            }
            self.configureBasicCell(self.commentSizingCell!, atIndexPath: indexPath)
            self.commentSizingCell?.bounds = CGRect(x: 0, y: 0, width: CGRectGetWidth(self.tableView.bounds), height: CGRectGetHeight(self.commentSizingCell!.bounds))
            self.commentSizingCell?.setNeedsLayout()
            self.commentSizingCell?.layoutIfNeeded()
            
            var size:CGSize = self.commentSizingCell!.contentView.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize)
            return size.height + 1
        }
    }
    
    // MARK: - Coffee Shop Header Protocol
    
    func userChangedView(pickedView: CoffeeShopViewSegment) {
        self.currentView = pickedView
        self.tableView.reloadData()
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "rateReviewSegue" {
            let vc = segue.destinationViewController as! RateReviewViewController
            vc.cafeteria = self.shop!
        }
    }


}
