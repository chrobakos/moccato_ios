//
//  LandingPageViewController.swift
//  Moccato iOS
//
//  Created by Bjarki Sorens on 16/04/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import UIKit

class LandingPageViewController: UIViewController {

    @IBOutlet weak var slideImage: UIImageView!
    
    var currentPageIndex:NSInteger?
    var currentImageFile:NSString?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.slideImage.image = UIImage(named: currentImageFile as! String)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
