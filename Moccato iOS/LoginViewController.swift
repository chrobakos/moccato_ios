//
//  LoginViewController.swift
//  Moccato iOS
//
//  Created by Bjarki Sorens on 17/04/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import UIKit
import ReactiveCocoa
import SwiftyJSON

class LoginViewController: UIViewController, UIBarPositioningDelegate {

    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    var loginLogoutDelegate:LoginLogoutDelegate?
    
    let dataManager = Datamanager(managedObjectContext: Factory.getManagedObjectContext())
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.loginLogoutDelegate = UIApplication.sharedApplication().delegate as? LoginLogoutDelegate
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func positionForBar(bar: UIBarPositioning) -> UIBarPosition {
        return UIBarPosition.TopAttached
    }

    @IBAction func backButtonPushed(sender: UIBarButtonItem) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    @IBAction func loginButtonPushed(sender: UIButton) {
        var hud = Loading.showLoadingInView(self.view, withText: "Logging in...")
        
        let loginSignal = Factory.getBackendService().loginUserWithEmail(self.emailTextField.text!, password: self.passwordTextField.text!)
        
        
        loginSignal.subscribeError { (error) -> Void in
            //if login fails
            let apiError:ServerApiError = error as! ServerApiError
            if apiError.statusCode == 401 || apiError.statusCode == 403 {
                Loading.hideLoading(hud)
                Loading.showError(self.view, withText: "Wrong email or password")
            }else{
                //if something else
                Loading.hideLoading(hud)
                Loading.showError(self.view, withText: "Server error: Unable to download user profile")
            }
        }
        
        
        loginSignal.flattenMap { (responseDict) -> RACStream! in
            // retrieve data from JSON
            let json = JSON(responseDict!)
            let token = json["token"].stringValue
            let user_id = json["user_id"].intValue
            
            //store token
            Authentication.storeToken(token)
            
            //download user info
            Loading.hideLoading(hud)
            hud = Loading.showLoadingInView(self.view, withText: "Loading user data...")
            let getUserInfoSignal = Factory.getBackendService().getUser(user_id)
            getUserInfoSignal.subscribeError({ (error) -> Void in
                //if downloading user data fils
                let apiError:ServerApiError = error as! ServerApiError
                Loading.hideLoading(hud)
                Loading.showError(self.view, withText: "Error loding user data: \(apiError.localizedMessage!)")
            })
            
            return getUserInfoSignal
        }.subscribeNext { (responseDict) -> Void in
            let json = JSON(responseDict!)
            let email = json["email"].stringValue
            let first_name = json["first_name"].stringValue
            let last_name = json["last_name"].stringValue
            // ...
            
            //save user info
            self.dataManager.saveUserInformation(first_name, email: email)
            
            Loading.hideLoading(hud)
            self.loginLogoutDelegate?.userDidFinishLogin(self)
        }
            
   
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    */
    
    
    

}
