//
//  User.swift
//  Moccato iOS
//
//  Created by Filip Strycko on 25/04/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import Foundation
import CoreData

@objc(User)
public class User: NSManagedObject {

    @NSManaged public var email: String
    @NSManaged public var id: NSNumber
    @NSManaged public var totalFreeCoffees: NSNumber
    @NSManaged public var first_name: String
    @NSManaged public var last_name: String
    @NSManaged public var favoriteCafeterias: NSSet
    @NSManaged public var ratings: NSSet

}
