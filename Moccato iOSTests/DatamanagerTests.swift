//
//  Moccato_iOSTests.swift
//  Moccato iOSTests
//
//  Created by Bjarki Sorens on 18/02/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import UIKit
import CoreData
import XCTest
import Moccato_iOS

class DatamanagerTests: XCTestCase {
    
    var dataManager: Datamanager?
    var context: NSManagedObjectContext?
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        var mock = DataMocker()
        self.context = mock.setupData()
        self.dataManager = Datamanager(managedObjectContext: self.context!)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testGettingCafeterias() {
        // This is an example of a functional test case.
        var cafeTerias = self.dataManager!.fetchCafeterias()
        println(cafeTerias)
        XCTAssert(cafeTerias.count == 2, "Failed getting all cafeterias")
    }
    
    func testGettingFavoriteCafeterias() {
        var favoriteCafeterias = self.dataManager!.fetchFavourited()
        XCTAssert(favoriteCafeterias.count == 1, "Failed getting favorite caffeterias")
    }
    
    func testSearchingForCafeterias() {
        var terias = self.dataManager!.fetchCafeteriasGivenSearchText("awesome")
        XCTAssert(terias?.count == 1, "Failed searching for cafeterias")
        XCTAssert(terias?.first?.id == 1, "Failed searching for cafeterias")
        
        terias = self.dataManager!.fetchCafeteriasGivenSearchText("cafeteria")
        XCTAssert(terias?.count == 2, "Failed searching for cafeterias")
    }
    
    func testGettingUser() {
        var user = self.dataManager!.fetchCurrentUserData()
        XCTAssert(user?.first_name == "Bjarki", "Failed getting user")
    }
    
    /*
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }
    */
    
}
