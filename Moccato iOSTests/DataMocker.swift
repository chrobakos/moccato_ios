//
//  DataMocker.swift
//  Moccato iOS
//
//  Created by Bjarki Sorens on 28/04/15.
//  Copyright (c) 2015 Moccato. All rights reserved.
//

import Foundation
import CoreData
import UIKit
import Moccato_iOS

class DataMocker {
    
    var managedObjectContext: NSManagedObjectContext?
    
    init() {
        let managedObjectModel = NSManagedObjectModel.mergedModelFromBundles([NSBundle.mainBundle()])!
        
        let persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        persistentStoreCoordinator.addPersistentStoreWithType(NSInMemoryStoreType, configuration: nil, URL: nil, options: nil, error: nil)
        
        let managedObjectContext = NSManagedObjectContext()
        managedObjectContext.persistentStoreCoordinator = persistentStoreCoordinator
        
        self.managedObjectContext = managedObjectContext
    }
    
    func setupData() -> NSManagedObjectContext {
        
        // lets create a user
        var user = NSEntityDescription.insertNewObjectForEntityForName("User", inManagedObjectContext: self.managedObjectContext!) as! User
        user.first_name = "Bjarki"
        
        // and then some cafeterias
        var cafeTeria1 = NSEntityDescription.insertNewObjectForEntityForName("Cafeteria", inManagedObjectContext: self.managedObjectContext!) as! Cafeteria
        cafeTeria1.id = 1
        cafeTeria1.name = "The Awesome Cafeteria"
        var cafeTeria2 = NSEntityDescription.insertNewObjectForEntityForName("Cafeteria", inManagedObjectContext: self.managedObjectContext!) as! Cafeteria
        cafeTeria2.id = 2
        cafeTeria2.name = "The Cafeteria For People Who Hate Coffee"
        
        // and lets make the user favorite the first cafeteria
        user.favoriteCafeterias = NSSet(array: [cafeTeria1])
        
        // finally return the context to be used in the test
        return self.managedObjectContext!
    }
    
}