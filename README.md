# Moccato iOS

The iOS application for Moccato. 

### Version
Alpha

### Installation

You need Cocoapods installed globally:

```sh
$ sudo gem install cocoapods 
$ pod setup
```
Then, install all the dependencies

```sh
$ pod install
```
and then finally you can open the `Moccato iOS.xcworkspace`.
